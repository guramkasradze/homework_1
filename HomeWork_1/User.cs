﻿using System;

namespace HomeWork_1
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }


        public User(int Id, string FirstName, string LastName, string Address)
        {
            this.Id = Id;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Address = Address;
        }

        public void GetMessage(Message message)
        {
            Console.WriteLine($"\n\nFrom: {message.From}\nTo: {this.FirstName}\nSubject: {message.Title}\n\n{message.Content}");
        }
    }
}
