﻿using System;

namespace HomeWork_1
{
    public class Message
    {
        public string From { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public Message(string From, string Title, string Content)
        {
            this.From = From;
            this.Title = Title;
            this.Content = Content;
        }
    }
}
