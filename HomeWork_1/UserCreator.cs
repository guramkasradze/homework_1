﻿using System;
using System.Collections.Generic;

namespace HomeWork_1
{
    public static class UserCreator
    {
        private static int Id = 0;
        public static List<User> Users { get; set; } = new List<User>();

        public static void AddUser(string name, string lastName, string address)
        {
            Users.Add(new User(++Id, name, lastName, address));
        }
    }
}
