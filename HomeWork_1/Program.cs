﻿using System;
using System.Linq;

namespace HomeWork_1
{
    class Program
    {
        static void Main(string[] args)
        {
            UserCreator.AddUser("Guram", "Kasradze", "Tbilisi");
            UserCreator.AddUser("Andrew", "Smith", "Batumi");
            UserCreator.AddUser("Tornike", "Gomareli", "Tbilisi");

            foreach (var user in UserCreator.Users)
            {
                Console.WriteLine($"{user.Id}) {user.FirstName}  {user.LastName}  {user.Address}");
            }

            var to = GetUserId();
            var message = GetMessageDetails();

            Post.SendMessage(to, message);
        }

        public static User GetUserId()
        {
            try
            {
                Console.Write("\nTo(Id): ");
                var enteredId = Convert.ToInt32(Console.ReadLine());
                var to = UserCreator.Users.First(i => i.Id == enteredId);

                return to;
            }
            catch(Exception ex)
            {
                Console.WriteLine($"{ex.Message}. Please Enter Valid User Id...");
            }

            return GetUserId();
        }

        public static Message GetMessageDetails()
        {
            Console.Write("From (Your Name): ");
            var myName = Console.ReadLine();
            Console.Write("Enter Subject: ");
            var newTitle = Console.ReadLine();
            Console.WriteLine("Enter Message:");
            var newContent = Console.ReadLine();

            return new Message(myName, newTitle, newContent);
        }
    }
}
